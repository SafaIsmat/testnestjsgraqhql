import Axios, { AxiosRequestConfig } from 'axios';
import { helper } from 'odoo-xmlrpc';//../odooHelper

type Model = 'juleb.order' | 'juleb.customer' | 'juleb.support' | 'product.template';

export const listAll = async (model: Model) => {
	const options = helper('juleb', model, 'search_read');
	const res = await Axios(options);
	return res.data.result;
};

export const read = async (model: Model, field: string, value) => {
	const options = helper('juleb', model, 'search_read', [[[field, '=', value]]]);
	const res = await Axios(options);
	return res.data.result;
};

export const getMany = async (model: Model, ids: Int32Array[]) => {
	const options = helper('juleb', model, 'read', [ids]);
	const res = await Axios(options);
	return res.data.result;
};
