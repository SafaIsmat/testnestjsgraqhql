/*import { ObjectId } from 'mongodb';
import { getProductList, getCustomerByExternalId, getCustomerByPhone,getOrderList, getOrderByBarcode } from './';
import { getCustomerByExternalId, getCustomerByPhone } from "../customer/customer.resolver";

interface Parent {
	_id: ObjectId;
}

export interface Resolver<T> {
	[key: string]: (parent: T & Parent, args?, context?, info?) => Promise<any> | any;
}

export const QueryResolver: Resolver<any> = {
	getCustomer: (_, args) => (args.id ? getCustomerByExternalId(args.id) : getCustomerByPhone(args.phone)),
	listProducts: () => getProductList(),
	listOrders: () => getOrderList(),
	// listOrders: () => getOrderList(),
	getOrderByBarcode: (_, args) => getOrderByBarcode(args.barcode),
};*/
