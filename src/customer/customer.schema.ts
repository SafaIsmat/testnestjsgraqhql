import {ObjectType, Field, Int, ID} from '@nestjs/graphql';

@ObjectType()
export class Customer {
    @Field(()=> ID)
    readonly id: number;
    @Field()
    readonly customer_id: string;
    @Field()
    readonly name: string;
    @Field()
    readonly email: string;//change to email address
    @Field()
    readonly address: string
    @Field()
    readonly phone: string
}


		
		
	
		
		