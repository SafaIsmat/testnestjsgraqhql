import { Resolver, Query , Args} from "@nestjs/graphql";
import { read } from 'odoo';
import {Customer} from "./customer.schema"


@Resolver()
export class CustomerResolver {
   
    @Query(() => Customer)
     async getCustomerByExternalId(@Args('id') id: string){
        const res = await read('juleb.customer', 'customer_id', id);
        return res[0];

    };

    @Query(() => Customer)
    async getCustomerBylId(@Args('id') id: number){
        const res = await read('juleb.customer', 'id', id);
        return res[0];
    };

    @Query(() => Customer)
     async getCustomerByPhone(@Args('phone') phone: string){
        const res = await read('juleb.customer', 'phone', phone);
        return res[0];

    };

    
}

    //export {getCustomerByExternalId, getCustomerBylId,getCustomerByPhone};
    
    




    
 
