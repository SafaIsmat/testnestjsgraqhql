import { Resolver, Query , Mutation, Args, ResolverOptions, Int} from "@nestjs/graphql";

import { ProductModule } from "./product.module";

/////
import {Product} from "./product.schema"
import {  OdooProduct } from './product.interface';
import { listAll, getMany } from 'odoo';
import { ResolveConfigOptions } from "prettier";
import { type } from "os";



//{ Resolver<>()


@Resolver()
export class ProductResolver {
	
    /*(OdooProduct: ResolverOptions)=>
    name_ar: (parent) => parent.j_name_ar,
	description: (parent) => parent.j_description,
	description_ar: (parent) => parent.j_description_ar,
	brand: (parent) => parent.j_brand,
	segment: (parent) => parent.j_segment,
	category: (parent) => find_category(parent.categ_id),
	price: (parent) => parent.j_price,
	rx_required: (parent) => parent.j_rx_required,
	featured: (parent) => parent.j_featured,
	new: (parent) => parent.j_new,
	hidden: (parent) => parent.j_hidden,
	status: (parent) => parent.j_status,
	image_front: (parent) => parent.image_1920,
	image_angle: (parent) => parent.j_image_angle,
	image_back: (parent) => parent.j_image_back,
	sfda: (parent) => parent.j_sfda,
	sku: (parent) => parent.j_sku,
	upc: (parent) => parent.j_upc,
	segment_2: (parent) => parent.j_segment_2,
	category_2: (parent) => find_category(parent.j_category_2),
	parent: (parent) => parent.j_parent,
	child: (parent) => parent.j_child,
	flavor: (parent) => parent.j_flavor,
	group: (parent) => parent.j_group,
	set: (parent) => parent.j_set,
	manufacture: (parent) => parent.j_manufacture,
	manufacture_country: (parent) => find_country(parent.j_manufacture_country),
	scent: (parent) => parent.j_scent,
	bottle_type: (parent) => parent.j_bottle_type,
	ingredients: (parent) => parent.j_ingredients,
	storage: (parent) => parent.j_storage,
	form: (parent) => parent.j_form,
	concentration: (parent) => parent.j_concentration,
	material: (parent) => parent.j_material,
	age: (parent) => parent.j_age,
	area: (parent) => parent.j_area,
	color: (parent) => parent.j_color,
	skin_type: (parent) => parent.j_skin_type,
	spf: (parent) => parent.j_spf,
	water_proof: (parent) => parent.j_water_proof,
	wireless: (parent) => parent.j_wireless,

    };*/
    

    //?
    @Query(() => Product)
    async getProductList ()  {
        const res = await listAll('product.template');
        return res;
    };


    
    //fix
    @Query(() => Product)
    async getProductsForOrder (@Args( '[ids]') ids: Int32Array[] ) {
        const res = await getMany('product.template', ids);
        return res;
    };
   



    @Query(()=> Product)
    async find_country(@Args('country_array') country_array: string){
        if (country_array) return country_array[1];
        else return null;
    };



    @Query(()=> Product)
    async find_category (@Args('category_array') category_array:string) {
        if (category_array) return category_array[1];
        else return null;
    };


}