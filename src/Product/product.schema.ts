import {ObjectType, Field, Int, ID, Float} from '@nestjs/graphql';

@ObjectType()
export class Product {
    @Field(()=> ID)
    readonly id: string;
    @Field()
    readonly name: string;
    @Field()
    readonly name_ar: string;
    @Field()
    readonly description: string;
    @Field()
    readonly description_ar: string;
    @Field()
    readonly brand: string;
    @Field()
    readonly segment: string;
    @Field()
    readonly category: string;
	@Field(() => Float)
    readonly price: number;
    @Field()
    readonly rx_required: boolean;
    @Field()
    readonly featured: boolean;
    @Field()
    readonly new: boolean;
    @Field()
    readonly hidden: boolean;
    @Field()
    readonly status: string;
    @Field()
    readonly image_front: string;
    @Field()
    readonly image_angle: string;
    @Field()
    readonly image_back: string;
    @Field()
    readonly sfda: string;
    @Field()
    readonly sku: string;
    @Field()
    readonly upc: string;
    @Field()
    readonly segment_2: string;
    @Field()
    readonly category_2: string;
    @Field()
    readonly parent: string;
    @Field()
    readonly child: string;
    @Field()
    readonly flavor: string;
    @Field()
    readonly group: string;
    @Field()
    readonly set: string;
    @Field()
    readonly manufacture: string;
    @Field()
    readonly manufacture_country: string;
    @Field()
    readonly scent: string;
    @Field()
    readonly bottle_type: string;
    @Field()
    readonly ingredients: string;
    @Field()
    readonly storage: string;
    @Field()
    readonly form: string;
    @Field()
    readonly concentration: string;
    @Field()
    readonly material: string;
    @Field()
    readonly age: string;
    @Field()
    readonly area: string;
    @Field()
    readonly color: string;
    @Field()
    readonly skin_type: string;
    @Field()
    readonly spf: string;
    @Field()
    readonly water_proof: string;
    @Field()
    readonly wireless: string;
    @Field()
    readonly qty_available: number;
}
   

@ObjectType()
export class OdooProduct {
    @Field(()=> ID)
    readonly id: string;
    @Field()
    readonly name: string;
    @Field()
    readonly j_name_ar: string;
    @Field()
    readonly j_description: string;
    @Field()
    readonly j_description_ar: string;
    @Field()
    readonly j_brand: string;
    @Field()
    readonly j_segment: string;
    @Field()
    readonly categ_id: string;
    @Field()
    readonly j_price: number;
    @Field()
    readonly j_rx_required: boolean;
    @Field()
    readonly j_featured: boolean;
    @Field()
    readonly j_new: boolean;
    @Field()
    readonly j_hidden: boolean;
    @Field()
    readonly j_status: string;
    @Field()
    readonly image_1920: string;
    @Field()
    readonly j_image_angle: string;
    @Field()
    readonly j_image_back: string;
    @Field()
    readonly j_sfda: string;
    @Field()
    readonly j_sku: string;
    @Field()
    readonly j_upc: string;
    @Field()
    readonly j_segment_2: string;
    @Field()
    readonly j_category_2: string;
    @Field()
    readonly j_parent: string;
    @Field()
    readonly j_child: string;
    @Field()
    readonly j_flavor: string;
    @Field()
    readonly j_group: string;
    @Field()
    readonly j_set: string;
    @Field()
    readonly j_manufacture: string;
    @Field()
    readonly j_manufacture_country: string;
    @Field()
    readonly j_scent: string;
    @Field()
    readonly j_bottle_type: string;
    @Field()
    readonly j_ingredients: string;
    @Field()
    readonly j_storage: string;
    @Field()
    readonly j_form: string;
    @Field()
    readonly j_concentration: string;
    @Field()
    readonly j_material: string;
    @Field()
    readonly  j_age: string;
    @Field()
    readonly j_area: string;
    @Field()
    readonly  j_color: string;
    @Field()
    readonly j_skin_type: string;
    @Field()
    readonly j_spf: string;
    @Field()
    readonly  j_water_proof: string;
    @Field()
    readonly j_wireless: string;
    @Field()
    readonly  qty_available: number;
        
    }
    
    /////??????
    type ProductStatus = 'NEW' | 'HIDDEN' | 'OUT_OF_STOCK';
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	



    
   
