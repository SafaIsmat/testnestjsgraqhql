import {ObjectType, Field, Int, ID} from '@nestjs/graphql';
import { Customer } from '../customer/customer.schema';
import { Product } from '../Product/product.schema';

@ObjectType()
export class Order {
    @Field(()=> ID)
    readonly id: number;
    @Field()
    readonly order_id: string;
    @Field()
    readonly customer: number;
    @Field()
    readonly customer_info: Customer;
    @Field(type => [Int])
    readonly products: number[];
    @Field()
    readonly product_info: Product;
}




