import { Customer } from '../customer/customer.schema';
import { Product } from '../Product/product.schema';


export interface Order {
	id: number;
	order_id: string;
	customer: number;
	customer_info: Customer;
	products: number[];
	product_info: Product;
}