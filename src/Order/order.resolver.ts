import { Resolver, Query , Mutation, Args, ResolverOptions} from "@nestjs/graphql";
import { listAll, read } from 'odoo';
import { Order } from "./order.schema";


@Resolver()
export class OrderResolver {
  

    @Query(() => Order)
    async getOrderByBarcode(@Args('barcode') barcode: string){
        const res = await read('juleb.order', 'order_id', barcode);
        return res[0];
    }


    @Query(() => [Order])
    async getOrderList(){
        const res = await listAll('juleb.order');
        return res;
    }
    

}