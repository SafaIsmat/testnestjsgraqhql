import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import {CustomerModule} from './customer/customer.module'
import { ProductModule } from './Product/product.module';
import { OrderModule } from './Order/order.module';

@Module({
  imports: [
    
    GraphQLModule.forRoot({autoSchemaFile: join(process.cwd(), 'src/schema.gql'),}),
    CustomerModule,
    ProductModule,
    OrderModule,
  ],
  
})

export class AppModule {}
